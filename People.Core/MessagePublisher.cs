using System.Threading.Tasks;

namespace People.Core {
    public interface MessagePublisher {
        Task Publish(object msg);
    }
}