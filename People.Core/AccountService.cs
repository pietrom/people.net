using System;

namespace People.Core {
    public class AccountService {
        private readonly AccountRepository repository;
        private readonly AccountIdGenerator idGenerator;

        public AccountService(AccountRepository repository, AccountIdGenerator idGenerator) {
            this.repository = repository;
            this.idGenerator = idGenerator;
        }

        public AccountId CreateAccount(CreateAccountCommand cmd) {
            Validate(cmd);
            var id = idGenerator.GenerateId();
            var account = new Account(id, cmd.Username, cmd.Password, cmd.Email);
            repository.Save(account);
            return id;
        }

        private void Validate(CreateAccountCommand cmd) {
            if (repository.Get(cmd.Username) != null) {
                throw new Exception("Username already taken");
            }
        }
    }

    public interface AccountIdGenerator {
        AccountId GenerateId();
    }

    public record AccountId(string Value);

    public record CreateAccountCommand(Username Username, Password Password, EmailAddress Email);
    // public interface CreateAccountCommand {
    //     public Username Username { get; }
    //     public Password Password { get; }
    //     public EmailAddress Email { get; }
    // }
}