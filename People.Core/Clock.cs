using System;

namespace People.Core {
    public interface Clock {
        public DateTimeOffset Now { get;  }
    }
}