using System;

namespace People.Core {
    public record Password {
        private Password(string value) {
            Value = value;
        }

        public string Value { get; }

        public static Password From(string value) {
            if (value.Length < 5) {
                throw new FormatException();
            }

            return new Password(value);
        }
    }
}