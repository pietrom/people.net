using System.Collections.Generic;

namespace People.Core {
    public interface AccountReadingGateway {
        IEnumerable<ReadOnlyAccount> FindAll();
    }

    public record ReadOnlyAccount(string Username, string email);
}