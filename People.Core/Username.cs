namespace People.Core {
    public record Username(string Value);
}