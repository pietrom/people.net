namespace People.Core {
    public interface MessageBus {
        void Start();
        void Stop();
    }
}