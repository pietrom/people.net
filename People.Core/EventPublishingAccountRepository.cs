namespace People.Core {
    public class EventPublishingAccountRepository : AccountRepository {
        private readonly AccountRepository decoratee;
        private readonly MessagePublisher publisher;

        public EventPublishingAccountRepository(AccountRepository decoratee, MessagePublisher publisher) {
            this.decoratee = decoratee;
            this.publisher = publisher;
        }

        public void Save(Account account) {
            decoratee.Save(account);
            foreach (var evt in account.PendingEvents) {
                publisher.Publish(evt);
            }
            account.Commit();
        }

        public Account Get(Username username) {
            return decoratee.Get(username);
        }
    }
}