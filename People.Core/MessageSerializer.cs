namespace People.Core {
    public interface MessageSerializer {
        string Serialize<T>(T msg);

        T Deserialize<T>(string text);
    }
}