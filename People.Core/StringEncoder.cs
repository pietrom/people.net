namespace People.Core {
    public interface StringEncoder {
        byte[] Encode(string text);
        
        string Decode(byte[] data);
    }
}