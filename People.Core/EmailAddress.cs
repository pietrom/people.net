using System;

namespace People.Core {
    public record EmailAddress {
        private EmailAddress(string value) {
            Value = value;
        }

        public string Value { get; }

        public static EmailAddress From(string value) {
            if (!value.Contains('@')) {
                throw new FormatException();
            }

            return new EmailAddress(value);
        }
    }
}