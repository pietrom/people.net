﻿using System;
using System.Collections.Generic;

namespace People.Core {
    public class Account {
        public Account(AccountId id, Username username, Password password, EmailAddress email) {
            Id = id;
            Username = username;
            Password = password;
            Email = email;
            Enabled = true;
            pendingEvents.Add(new AccountCreatedEvent(id, username, email));
        }

        public AccountId Id { get; }
        public Username Username { get; private set; }
        public Password Password { get; private set; }
        public EmailAddress Email { get; private set; }
        public bool Enabled { get; private set; }
        public DateTimeOffset LastChangedAt { get; private set; }
        private readonly List<DomainEvent<AccountId>> pendingEvents = new List<DomainEvent<AccountId>>();

        public IReadOnlyList<DomainEvent<AccountId>> PendingEvents => pendingEvents.AsReadOnly();

        public void ChangePassword(Password oldPwd, Password newPwd, DateTimeOffset when) {
            if (Password != oldPwd) {
                throw new Exception("Old password is wrong");
            }
            Password = newPwd;
            LastChangedAt = when;
            pendingEvents.Add(new PasswordChangedEvent(Id));
        }

        public void Commit() {
            pendingEvents.Clear();
        }
    }

    public record DomainEvent<TId> {
        public DomainEvent(TId id) {
            Id = id;
        }

        public TId Id { get; }
    }

    public record AccountCreatedEvent : DomainEvent<AccountId> {
        public AccountCreatedEvent(AccountId id, Username username, EmailAddress email) : base(id) {
            Username = username;
            Email = email;
        }

        public Username Username { get; }
        public EmailAddress Email { get; }
    }

    public record PasswordChangedEvent : DomainEvent<AccountId> {
        public PasswordChangedEvent(AccountId id) : base(id) {
        }
    };
}