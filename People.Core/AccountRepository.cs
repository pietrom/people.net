namespace People.Core {
    public interface AccountRepository {
        void Save(Account account);

        Account Get(Username username);
    }
}