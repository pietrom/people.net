using System.Linq;
using Microsoft.AspNetCore.Mvc;
using People.Core;

namespace People.Api {
    [ApiController]
    [Route("people")]
    public class AccountController : ControllerBase {
        private readonly AccountService service;
        private readonly AccountReadingGateway readingGateway;

        public AccountController(AccountService service, AccountReadingGateway readingGateway) {
            this.service = service;
            this.readingGateway = readingGateway;
        }

        [HttpPost]
        public IActionResult Create(CreateAccountDto dto) {
            var id = service.CreateAccount(dto.ToCommand());
            // service.CreateAccount(dto);
            return Created($"/people/{id.Value}", new { });
        }

        [HttpGet]
        public IActionResult GetAll() {
            var accounts = readingGateway.FindAll();
            return Ok(accounts.Select(x => new GetAllDto(x.Username, x.email)));
        }
    }

    public record GetAllDto(string Username, string Email) {
        public string Initials => $"{Username[0]}{Email[0]}";
    };

    public record CreateAccountDto(string User, string Pwd, string Email) {
        public CreateAccountCommand ToCommand() {
            var username = new Username(User);
            var password = Password.From(Pwd);
            var email = EmailAddress.From(Email);
            return new CreateAccountCommand(username, password, email);
        }
    }
    /*
     public record CreateAccountDto(string User, string Pwd, string Email) : CreateAccountCommand {
        public Username Username => new Username(User);
        public Password Password => Password.From(Pwd);
        EmailAddress CreateAccountCommand.Email => EmailAddress.From(Email);
    }
     */
    /*
     { user: "pietrom", pwd: "aaa111", email: "pmartinelli@example.com", "user-name": "dkjgdj" }
     */
}