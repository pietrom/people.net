using System.Collections.Generic;
using System.Linq;
using People.Core;

namespace People.InMemoryPersistence {
    public class InMemoryAccountRepository : AccountRepository {
        public void Save(Account account) {
            InMemoryAccountStore.Users[account.Id] = account;
        }

        public Account Get(Username username) {
            var found = InMemoryAccountStore.Users.Where(x => x.Value.Username == username).ToList();
            return found.Any() ? found.First().Value : null;
        }
    }
    
    public class InMemoryAccountReadingGateway : AccountReadingGateway {
        public IEnumerable<ReadOnlyAccount> FindAll() {
            return InMemoryAccountStore.Users.Values.Select(x => new ReadOnlyAccount(x.Username.Value, x.Email.Value));
        }
    }
}