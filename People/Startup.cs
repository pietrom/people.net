using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using People.Core;
using People.InMemoryPersistence;
using People.MongoDb;
using People.RabbitMq;
using People.System;

namespace People {
    public class Startup {
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.AddSingleton<MessageSerializer, JsonSerializer>();
            services.AddSingleton<StringEncoder, Utf8Encoder>();
            services.AddSingleton<AccountService>();
            services.AddSingleton<AccountIdGenerator, GuidAccountIdGenerator>();
            services.AddSingleton<Clock, SystemClock>();
            // services.AddSingleton<AccountRepository, InMemoryAccountRepository>();
            // services.AddSingleton<AccountReadingGateway, InMemoryAccountReadingGateway>();
            services.AddSingleton<MongoDbAccountRepository>();
            services.AddSingleton<AccountRepository>(x => x.GetRequiredService<MongoDbAccountRepository>());
            services.Decorate<AccountRepository, EventPublishingAccountRepository>();
            services.AddSingleton<AccountReadingGateway>(x => x.GetRequiredService<MongoDbAccountRepository>());
            services.AddSingleton<MongoDbDatabase>();
            services.AddSingleton(new MongoDbConfiguration("mongodb://localhost/people", "people"));
            services.AddSingleton(new RabbitMqConfiguration("localhost", 5672, "people.net", "guest", "guest"));

            services.AddSingleton<MessageBus, RabbitMqBus>();
            services.AddSingleton<RabbitMqMessagePublisher>();
            services.AddSingleton<MessagePublisher>(x => x.GetRequiredService<RabbitMqMessagePublisher>());
            services.AddSingleton<RabbitMqConsumer, RabbitMqConsumer<CreateAccountMessage>>(x =>
                ActivatorUtilities.CreateInstance<RabbitMqConsumer<CreateAccountMessage>>(x,
                    RabbitMqConsumerConfiguration
                        .Builder
                        .WithDefaultQueue<CreateAccountMessage>()
                        .BindDefaultExchange<CreateAccountMessage>()
                        .Build()
                )
            );
            services.AddSingleton<MessageHandler<CreateAccountMessage>, CreateAccountMessageHandler>();

            services.AddControllers();
            services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new OpenApiInfo { Title = "People", Version = "v1" }); });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env,
            IHostApplicationLifetime applicationLifetime, MessageBus messageBus) {
            applicationLifetime.ApplicationStopping.Register(() => { messageBus.Stop(); });

            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "People v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            messageBus.Start();
        }
    }

    public static class ServiceCollectionExtensions {
        public static void Decorate<TInterface, TDecorator>(this IServiceCollection services)
            where TInterface : class
            where TDecorator : class, TInterface {
            // grab the existing registration
            var wrappedDescriptor = services.FirstOrDefault(
                s => s.ServiceType == typeof(TInterface));

            // check it&#039;s valid
            if (wrappedDescriptor == null)
                throw new InvalidOperationException($"{typeof(TInterface).Name} is not registered");

            // create the object factory for our decorator type,
            // specifying that we will supply TInterface explicitly
            var objectFactory = ActivatorUtilities.CreateFactory(
                typeof(TDecorator),
                new[] { typeof(TInterface) });

            // replace the existing registration with one
            // that passes an instance of the existing registration
            // to the object factory for the decorator
            services.Replace(ServiceDescriptor.Describe(
                typeof(TInterface),
                s => (TInterface)objectFactory(s, new[] { s.CreateInstance(wrappedDescriptor) }),
                wrappedDescriptor.Lifetime)
            );
        }

        private static object CreateInstance(this IServiceProvider services, ServiceDescriptor descriptor) {
            if (descriptor.ImplementationInstance != null)
                return descriptor.ImplementationInstance;

            if (descriptor.ImplementationFactory != null)
                return descriptor.ImplementationFactory(services);

            return ActivatorUtilities.GetServiceOrCreateInstance(services, descriptor.ImplementationType);
        }
    }
}