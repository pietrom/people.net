using System;
using People.Core;

namespace People {
    class SystemClock : Clock {
        public DateTimeOffset Now => DateTimeOffset.Now;
    }
}