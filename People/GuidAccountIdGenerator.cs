using System;
using People.Core;

namespace People {
    public class GuidAccountIdGenerator : AccountIdGenerator {
        public AccountId GenerateId() {
            return new AccountId(Guid.NewGuid().ToString());
        }
    }
}