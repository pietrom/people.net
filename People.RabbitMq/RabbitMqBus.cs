using System.Collections.Generic;
using People.Core;
using RabbitMQ.Client;

namespace People.RabbitMq {
    public class RabbitMqBus : MessageBus {
        private readonly RabbitMqConfiguration config;
        private readonly StringEncoder encoder;
        private readonly MessageSerializer serializer;
        private IConnection connection;
        private readonly IEnumerable<RabbitMqConsumer> consumers;
        private readonly RabbitMqMessagePublisher publisher;

        public RabbitMqBus(RabbitMqConfiguration config, StringEncoder encoder, MessageSerializer serializer, IEnumerable<RabbitMqConsumer> consumers, RabbitMqMessagePublisher publisher) {
            this.config = config;
            this.encoder = encoder;
            this.serializer = serializer;
            this.consumers = consumers;
            this.publisher = publisher;
        }

        public void Start() {
            ConnectionFactory factory = new ConnectionFactory();
            factory.UserName = config.Username;
            factory.Password = config.Password;
            factory.VirtualHost = config.VirtualHost;
            factory.HostName = config.Host;
            factory.Port = config.Port;
            // factory.ClientProvidedName = "app:people.net";
            factory.DispatchConsumersAsync = true;

            connection = factory.CreateConnection();
            
            publisher.Start(connection);
            
            foreach (var consumer in consumers) {
                consumer.Start(connection);
            }
        }

        public void Stop() {
            foreach (var consumer in consumers) {
                consumer.Stop();
            }
            
            publisher.Stop();
            
            connection?.Close();
        }
    }
}