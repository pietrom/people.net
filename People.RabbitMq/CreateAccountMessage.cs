using System.Threading.Tasks;
using People.Core;

namespace People.RabbitMq {
    public record CreateAccountMessage(
        string Username,
        string Password,
        string Email
    );

    public class CreateAccountMessageHandler : MessageHandler<CreateAccountMessage> {
        private readonly AccountService service;

        public CreateAccountMessageHandler(AccountService service) {
            this.service = service;
        }

        public Task Handle(CreateAccountMessage msg) {
            var command = new CreateAccountCommand(new Username(msg.Username), Password.From(msg.Password),
                EmailAddress.From(msg.Email));
            service.CreateAccount(command);
            return Task.CompletedTask;
        }
    }
}