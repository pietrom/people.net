using System.Threading.Tasks;

namespace People.RabbitMq {
    public interface MessageHandler<TMsg> {
        Task Handle(TMsg msg);
    }
}