namespace People.RabbitMq {
    public class RabbitMqConsumerConfiguration {
        public string QueueName { get; }
        public string? ExchangeName { get; }
        public bool BindExchange => ExchangeName != null;

        private RabbitMqConsumerConfiguration(string queueName, string exchangeName = null) {
            QueueName = queueName;
            ExchangeName = exchangeName;
        }
        
        public static RabbitMqConsumerConfigurationBuilder Builder => new RabbitMqConsumerConfigurationBuilder();

        public class RabbitMqConsumerConfigurationBuilder {
            private string queueName;
            private string exchangeName;
            
            public RabbitMqConsumerConfigurationBuilder WithDefaultQueue<T>() {
                return WithQueue(typeof(T).Name);
            }

            public RabbitMqConsumerConfigurationBuilder WithQueue(string queueName) {
                this.queueName = queueName;
                return this;
            }

            public RabbitMqConsumerConfigurationBuilder BindDefaultExchange<T>() {
                return BindExchange(typeof(T).Name);
            }

            public RabbitMqConsumerConfigurationBuilder BindExchange(string exchangeName) {
                this.exchangeName = exchangeName;
                return this;
            }

            public RabbitMqConsumerConfiguration Build() => new RabbitMqConsumerConfiguration(queueName, exchangeName);
        }
    }
}