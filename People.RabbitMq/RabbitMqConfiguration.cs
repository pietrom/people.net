namespace People.RabbitMq {
    public record RabbitMqConfiguration(
        string Host,
        int Port,
        string VirtualHost,
        string Username,
        string Password
    );
}