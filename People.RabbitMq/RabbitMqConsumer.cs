﻿using System;
using System.Threading.Tasks;
using People.Core;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace People.RabbitMq {
    public interface RabbitMqConsumer {
        void Start(IConnection connection);
        void Stop();
    }

    public class RabbitMqConsumer<TMsg> : RabbitMqConsumer {
        private readonly MessageHandler<TMsg> handler;
        private readonly StringEncoder encoder;
        private readonly MessageSerializer serializer;
        private readonly RabbitMqConsumerConfiguration cfg;
        private IModel channel;
        private string consumerTag;

        public RabbitMqConsumer(MessageHandler<TMsg> messageHandler, StringEncoder encoder, MessageSerializer serializer, RabbitMqConsumerConfiguration cfg) {
            this.handler = messageHandler;
            this.encoder = encoder;
            this.serializer = serializer;
            this.cfg = cfg;
        }

        public void Start(IConnection connection) {
            channel = connection.CreateModel();
            channel.QueueDeclare(cfg.QueueName, true, false, false);
            if (cfg.BindExchange) {
                channel.ExchangeDeclare(cfg.ExchangeName, "direct", true, false);
                channel.QueueBind(cfg.QueueName, cfg.ExchangeName, string.Empty);
            }

            var consumer = new AsyncEventingBasicConsumer(channel);
            consumer.Received += async (ch, ea) =>
            {
                var body = ea.Body.ToArray();
                string text = encoder.Decode(body);
                try {
                    TMsg msg = serializer.Deserialize<TMsg>(text);
                    await handler.Handle(msg);
                    channel.BasicAck(ea.DeliveryTag, false);
                } catch (Exception e) {
                    Console.WriteLine($"Error consuming message\n{text}\n{e.Message}");
                    channel.BasicNack(ea.DeliveryTag, false, false);
                }
                Task.Yield();
            };
            consumerTag = channel.BasicConsume(cfg.QueueName, false, consumer);
        }

        public void Stop() {
            channel?.BasicCancel(consumerTag);
        }
    }
}