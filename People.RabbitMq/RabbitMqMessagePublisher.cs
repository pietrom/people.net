using System.Threading.Tasks;
using People.Core;
using RabbitMQ.Client;

namespace People.RabbitMq {
    public class RabbitMqMessagePublisher : MessagePublisher {
        private readonly StringEncoder encoder;
        private readonly MessageSerializer serializer;
        private IModel channel;

        public RabbitMqMessagePublisher(StringEncoder encoder, MessageSerializer serializer) {
            this.encoder = encoder;
            this.serializer = serializer;
        }

        public void Start(IConnection connection) {
            channel = connection.CreateModel();
        }

        public void Stop() {
            channel?.Close();
        }

        public Task Publish(object msg) {
            var text = serializer.Serialize(msg);
            var data = encoder.Encode(text);
            var exchangeName = msg.GetType().Name;
            channel.ExchangeDeclare(exchangeName, "direct", true, false);
            channel.BasicPublish(exchangeName, string.Empty, null, data);
            return Task.CompletedTask;
        }
    }
}