namespace People.RabbitMq {
    public record AccountCreatedMessage(
        string Id, string Username, string Password, string Email
    );
}