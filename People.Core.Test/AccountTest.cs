using System;
using NUnit.Framework;

namespace People.Core.Test {
    public class AccountTest {
        private static readonly AccountId Id = new AccountId("test");
        
        [SetUp]
        public void Setup() {
        }

        [Test]
        public void CanChangeAccountPasswordProvidingTheCurrentOne() {
            var original = Password.From("abc123");
            var desired = Password.From("xyz987");
            var account = new Account(Id, new Username("pietrom"), original,
                EmailAddress.From("pietrom@codiceplastico.com"));
            account.ChangePassword(original, desired, DateTimeOffset.Now);
            Assert.That(account.Password, Is.EqualTo(desired));
        }
        
        [Test]
        public void CantChangeAccountPasswordWhenOldPasswordIsWrong() {
            var original = Password.From("abc123");
            var desired = Password.From("xyz987");
            var account = new Account(Id, new Username("pietrom"), original,
                EmailAddress.From("pietrom@codiceplastico.com"));
            Assert.Throws<Exception>(() => account.ChangePassword(desired, desired, DateTimeOffset.Now));
        }
    }
}