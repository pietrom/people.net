using System.Text;
using People.Core;

namespace People.System {
    public class Utf8Encoder : StringEncoder {
        public byte[] Encode(string text) {
            return Encoding.UTF8.GetBytes(text);
        }

        public string Decode(byte[] data) {
            return Encoding.UTF8.GetString(data);
        }
    }
}