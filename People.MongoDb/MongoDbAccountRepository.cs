﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;
using People.Core;

namespace People.MongoDb {
    public record MongoDbConfiguration(string ConnectionString, string DatabaseName);

    public class MongoDbDatabase {
        private readonly MongoClient client;
        private readonly IMongoDatabase database;

        public MongoDbDatabase(MongoDbConfiguration cfg) {
            client = new MongoClient(cfg.ConnectionString);
            database = client.GetDatabase(cfg.DatabaseName);
        }

        public IMongoCollection<T> GetCollection<T>(string collectionName) {
            return database.GetCollection<T>(collectionName);
        }
    }

    public class MongoDbAccountRepository : AccountRepository, AccountReadingGateway {
        private readonly IMongoCollection<PersistentAccount> collection;

        public MongoDbAccountRepository(MongoDbDatabase db) {
            collection = db.GetCollection<PersistentAccount>("account");
        }

        public void Save(Account account) {
            collection.InsertOne(new PersistentAccount {
                Id = account.Id.Value,
                Username = account.Username.Value,
                Password = account.Password.Value,
                Email = account.Email.Value
            });
        }

        public Account Get(Username username) {
            var found = collection.FindSync(Builders<PersistentAccount>.Filter.Eq(x => x.Username, username.Value))
                .FirstOrDefault();
            return found == null
                ? null
                : new Account(
                    new AccountId(found.Id),
                    new Username(found.Username),
                    Password.From(found.Password),
                    EmailAddress.From(found.Email)
                );
        }

        public IEnumerable<ReadOnlyAccount> FindAll() {
            var items = collection.FindSync(Builders<PersistentAccount>.Filter.Empty).ToList();
            return items.Select(x => new ReadOnlyAccount(x.Username, x.Email));
        }
    }

    internal class PersistentAccount {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }
}